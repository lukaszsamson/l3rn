"use strict";

Template.questionCreator.helpers({
  stateClass() {
    const selectedImages = this.imageSelectors.filter(p => p.image).length;
    return selectedImages === 0 ? "noneSelected" : selectedImages === 1 ? "oneSelected" : "allSelected";
  },
  imagesNotChosen() {
    return !this.imageSelectors.every(p => p.image);
  },
  disableCreate() {
    return !this.name || this.imageSelectors[0].image === this.imageSelectors[1].image;
  },
  imagesAreSame() {
    return this.imageSelectors[0].image === this.imageSelectors[1].image;
  },
  creating() {
    return Session.get("questionCreator.creating");
  }
});

Template.questionCreator.events({
  "change input[name='name']"(e) {
    Session.set("questionCreator.name", $(e.target).val());
  },
  "click button[name='swap']"() {
    const imageSelectors = Session.get("questionCreator.imageSelectors");
    const tmpImage = imageSelectors[0].image;
    const tmpDescription = imageSelectors[0].description;
    imageSelectors[0].image = imageSelectors[1].image;
    imageSelectors[0].description = imageSelectors[1].description;

    imageSelectors[1].image = tmpImage;
    imageSelectors[1].description = tmpDescription;

    //todo optimize
    imageSelectors[0].width = null;
    imageSelectors[0].height = null;
    imageSelectors[1].width = null;
    imageSelectors[1].height = null;

    Session.set("questionCreator.imageSelectors", imageSelectors);
  },
  "click button[name='create']"() {
    Session.set("questionCreator.creating", true);
    Meteor.call("create", Session.get("questionCreator.name"), Session.get("questionCreator.imageSelectors").map(p => {
      return {
        description: p.description,
        id: p.id,
        imageId: p.imageId
      };
    }), (error, questionId) => {
      Session.set("questionCreator.creating", false);
      if (!error) {
        Router.go("question", {
          _id: questionId
        });
        Session.set("questionCreator.name", "");
        Session.set("questionCreator.imageSelectors", app.getDefaultImageSelectors());
        Session.set("questionCreator.progressCurrentHeader", 1);
      }
      else {
        //todo errorhandling
        throw error;
      }
    });

  }
});



Template.imageSelector.helpers({
  stateClass() {
    return Session.get("questionCreator.imageSelectors")[this.id].image ? "imageSelected" : "imageNotSelected";
  },
  src() {
    return Session.get("questionCreator.imageSelectors")[this.id].image;
  },
  heightStyle() {
    const imageGetter = () => Session.get("questionCreator.imageSelectors")[this.id].image;
    return app.heightStyle("questionCreator.imageSelectors", this.id, imageGetter);
  }
});

Template.imageSelector.events({
  "change input[type='file']"(e) {
    const file = e.target.files[0];
    if (!file) {
      return;
    }
    const reader = new FileReader();
    reader.onloadend = (onloadEvent) => {
      if (onloadEvent.target.error) {
        //todo errorhandling
        throw onloadEvent.target.error;
      } else {
        const imageSelectors = Session.get("questionCreator.imageSelectors");
        imageSelectors[this.id].image = onloadEvent.target.result;
        imageSelectors[this.id].height = null;
        imageSelectors[this.id].width = null;
        Session.set("questionCreator.imageSelectors", imageSelectors);

        Session.set("questionCreator.progressCurrentHeader", Session.get("questionCreator.progressCurrentHeader") + 1);
      }
    };
    reader.readAsDataURL(file);

    FS.Utility.eachFile(e, (f) => {
      app.Images.insert(f, (error, fileObj) => {
        if (error) {
          //todo errorhandling
          throw error;
        } else {
          const imageSelectors = Session.get("questionCreator.imageSelectors");
          imageSelectors[this.id].imageId = fileObj._id;
          Session.set("questionCreator.imageSelectors", imageSelectors);
        }
        // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
      });
    });
  },
  "click .imageSelector .img"(e) {
    if (!this.image && e.target.nodeName.toLowerCase() !== "input") {
      $(e.currentTarget).parent().find("input").click();
    }
  },
  "click .imageSelector button"(e) {
    $(e.target).parent().parent().find("input").click();
  },
  "change input[name='description']"(e) {
    const value = $(e.target).val();
    const imageSelectors = Session.get("questionCreator.imageSelectors");
    imageSelectors[this.id].description = value;
    Session.set("questionCreator.imageSelectors", imageSelectors);
  }
});
