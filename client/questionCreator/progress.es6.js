"use strict";

Session.setDefault("questionCreator.progressCurrentHeader", 1);

Template.progress.helpers({
  headerClass(i) {
    const current = Session.get("questionCreator.progressCurrentHeader");
    return i < current ? "done" : i === current ? "active" : "";
  }
});
