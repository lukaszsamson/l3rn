"use strict";

const isOwned = question => Meteor.userId() === question.owner;

Template.question.helpers({
  isOwned() {
    return isOwned(this.question);
  }
});

Template.question.events({

  "click button[name='delete']"() {
    Meteor.call("remove", this.question._id, (result, error) => {
      if (!error) {
        Router.go("myQuestions");
      }
      else {
        //todo errorhandling
        throw error;
      }
    });
  },
  "click button[name='close']"() {
    Meteor.call("close", this.question._id);
  }
});

Template.privacySettings.helpers({
  isPublic() {
    return Session.get("privacySettings.isPublic");
  },
  isDiscoverable() {
    return Session.get("privacySettings.isDiscoverable");
  },
  isDiscoverableDisabled() {
    return !Session.get("privacySettings.isPublic") || this.question.isClosed;
  },
  editMode() {
    return Session.get("privacySettings.editMode");
  }
});

Template.privacySettings.events({
  "click button[name='edit']"() {
    Session.set("privacySettings.editMode", true);
    Session.set("privacySettings.isPublic", this.question.isPublic);
    Session.set("privacySettings.isDiscoverable", this.question.isDiscoverable);
  },
  "change input[name='isPublic']"(e) {
    const isPublic = e.target.checked;
    Session.set("privacySettings.isPublic", isPublic);
    if (!isPublic) {
      Session.set("privacySettings.isDiscoverable", false);
    }
  },
  "change input[name='isDiscoverable']"(e) {
    const isDiscoverable = e.target.checked;
    Session.set("privacySettings.isDiscoverable", isDiscoverable);
  },
  "submit .privacySettings"(e) {
    e.preventDefault();
    Meteor.call("updatePrivacySettings", this.question._id, {
      isPublic: !!Session.get("privacySettings.isPublic"),
      isDiscoverable: !!Session.get("privacySettings.isDiscoverable")
    }, (error) => {
      if (error) {
        //todo errorhandling
        throw error;
      }
      Session.set("privacySettings.editMode", false);
    });
  }
});

Template.option.helpers({
  canVote: () => !Template.parentData().vote && !Template.parentData().question.isClosed,
  isClosed: () => Template.parentData().question.isClosed,
  canSeeVotes: () => Template.parentData().vote || isOwned(Template.parentData().question),
  votedFor() {
    const vote = Template.parentData().vote;
    return vote && vote.optionId === this.id;
  },
  srcAttr() {
    return {
      src: this.imageFile ? this.imageFile.url({store: "imagesOptimized"}) : ""
    };
  }
});

Template.option.events({
  "click button"() {
    Meteor.call("vote", Template.parentData().question._id, this.id);
  }
});
