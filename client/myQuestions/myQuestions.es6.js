"use strict";

const value2Bool = (value) => value === "true" ? true : (value === "false" ? false : undefined);

const isChecked = (value, name) => {
  const r = Session.equals(`myQuestions.${name}`, value2Bool(value));
  return r ? "checked" : "";
};

Template.myQuestions.helpers({
  triStateRadio(name, legend, ...labels) {
    const values = ["", "true", "false"];
    let inputs = "";
    for (const i in values) {
      inputs += `<label><input type="radio" name="${name}" value="${values[i]}" ${isChecked(values[i], name)}/>${labels[i]}</label>`;
    }

    const res = `<fieldset class="triStateRadio"><legend>${legend}</legend>${inputs}</fieldset>`;
    return res;
  }
});

Template.myQuestions.events({
  "change input[type='radio']"(e) {
    const value = value2Bool(e.target.value);
    const name = e.target.name;
    Session.set(`myQuestions.${name}`, value);
  }
});

Template.thumbnail.helpers({
  thumbnailSrc() {
    const imageFile = app.Images.findOne({_id: this.imageId});
    return imageFile ? imageFile.url({store: "thumbs"}) : "";
  }
});
