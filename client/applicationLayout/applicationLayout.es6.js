"use strict";

const classes = ["menu", "login"];
const hideMenus = () => classes.forEach(c => Session.set(`applicationLayout.${c}Shown`, false));

Template.applicationLayout.helpers({
  navStateClass() {
    const cl = classes.filter(c => Session.equals(`applicationLayout.${c}Shown`, true)).map(c => c + "Shown");
    return cl.join(" ");
  },

  userDisplayName() {
    const user = Meteor.user();
    if (!user) {
      return "";
    }
    return user.profile.name;
  }
});

Template.applicationLayout.events({
  "click .navbarButton>a"(e) {
    e.preventDefault();
    //IE10 hack
    const cl = e.currentTarget.parentElement.dataset ? e.currentTarget.parentElement.dataset.role : e.currentTarget.parentElement.getAttribute("data-role");
    const shown = Session.equals(`applicationLayout.${cl}Shown`, true);
    Session.set(`applicationLayout.${cl}Shown`, !shown);
    classes.filter(c => c !== cl).forEach(c => Session.set(`applicationLayout.${c}Shown`, false));
  },
  "click nav li>a"() {
    hideMenus();
  },
  "click [name='loginWithFacebook']"(e) {
    e.preventDefault();
    //todo handle error
    Meteor.loginWithFacebook({}, (err) => {
      if (err) {
        throw new Meteor.Error("Facebook login failed");
      }
      hideMenus();
    });
  },
  "click [name='logout']"(e) {
    e.preventDefault();
    //todo handle error
    Meteor.logout((err) => {
      if (err) {
        throw new Meteor.Error("Logout failed");
      }
      hideMenus();
    });
  }
});
