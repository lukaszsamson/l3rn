"use strict";

Session.setDefault("home.currentSlide", 1);

Template.home.onCreated(function() {
  this.touchStartX = new ReactiveVar(0);
  this.translationX = new ReactiveVar(0);

  this.lastTranslationX = new ReactiveVar(0);

  this.lockTranslationX = new ReactiveVar(0);
  this.translationLocked = new ReactiveVar(false);

  this.lastT = new ReactiveVar(new Date().getTime());
  this.lastV = new ReactiveVar(0);

  this.X0 = 0;
  this.V0 = 0;
  this.T0 = 0;

  this.startAnimation = () => {
    this.X0 = this.translationX.get();
    this.V0 = this.lastV.get();
    //console.log(this.V0);
    this.T0 = new Date().getTime();
  };

  this.changeSlide = (delta) => {
    const current = Session.get("home.currentSlide");
    const newSlide = current + delta;
    if (newSlide > 0 && newSlide <= Session.get("home.slidesLength")) {
      Session.set("home.currentSlide", newSlide);
      this.startAnimation();
    }
  };
});

Template.home.onRendered(function() {
  const slideHeaders = this.findAll(".slideContainer .slide h1");
  const slider = this.find(".slideContainer .slider");

  slider.onmousewheel = (e) => {
    if (e.wheelDelta > 0) {
      this.changeSlide(-1);
    }
    if (e.wheelDelta < 0) {
      this.changeSlide(1);
    }
  };

  for (let i = 0; i < slideHeaders.length; i++) {
    Session.set("home.slide." + (i + 1), slideHeaders[i].id);
  }
  Session.set("home.slidesLength", slideHeaders.length);

  const setCurrent = () => {
    const href = window.location.hash.split("#")[1];
    const current = Session.get("home.currentSlide");
    for (let i = 0; i < slideHeaders.length; i++) {
      if (slideHeaders[i].id === href) {
        if (current !== i + 1) {
          Session.set("home.currentSlide", i + 1);
          //this.startAnimation();
        }
        return;
      }
    }
    if (current !== 1) {
      Session.set("home.currentSlide", 1);
      //this.startAnimation();
    }
  };

  this.hashchangeHandler = () => {
    this.find(".slideContainer").scrollLeft = 0;//todo needed?
    setCurrent();
  };

  window.addEventListener("hashchange", this.hashchangeHandler, false);
  setCurrent();

  this.keyupHandler = (e) => {
    if (e.keyCode === 37) {
      this.changeSlide(-1);
    }
    if (e.keyCode === 39) {
      this.changeSlide(1);
    }
  };

  window.addEventListener("keyup", this.keyupHandler, false);

  Tracker.autorun(() => {
    if (Session.equals("translationLocked", false)) {
      //console.log("starting anim");
      Tracker.nonreactive(this.startAnimation);
      Tracker.nonreactive(() => {
        var current = Session.get("home.currentSlide");
        if (current > 1 && this.V0 > 0) {
          this.changeSlide(-1);
        }
        if (current < Session.get("home.slidesLength") && this.V0 < 0) {
          this.changeSlide(1);
        }
      });

    } else {
      //console.log("locking");
      this.lastV.set(0);
    }
  });

  this.autorun(() => {
    const currentSubscribe = Session.get("home.currentSlide");
    const href = window.location.hash.split("#")[1];
    if (href !== slideHeaders[currentSubscribe - 1].id) {
      window.history.pushState({}, "", "#" + slideHeaders[currentSubscribe - 1].id);
    }

    for (let i = 0; i < Session.get("home.slidesLength"); i++) {
      $(slideHeaders[i]).closest(".slide").toggleClass("active", i === currentSubscribe - 1);
    }

    Tracker.nonreactive(this.startAnimation);
  });

  this.updateSliderTranslation = (x) => {
    if (typeof x !== "number") {
      throw new Error("Expected number, got " + typeof x);
    }
    if (isNaN(x) || !isFinite(x)) {
      throw new Error("Expected valid number, got " + x);
    }

    const style = `translateX(${x}px)`;

    slider.style.webkitTransform = style;
    slider.style.transform = style;
  };

  this.requestedAnimationFrame = null;
  this.interval = null;
  this.autorun((c) => {
    if (c.firstRun) {
      this.interval = Meteor.setInterval(() => c.invalidate(), 50);
    }
    //this.translationLocked.get();
    Session.get("translationLocked");
    this.translationX.get();
    Session.get("home.currentSlide");

    if (this.requestedAnimationFrame) {
      cancelAnimationFrame(this.requestedAnimationFrame);
    }
    //console.log("requesting");
    this.requestedAnimationFrame = requestAnimationFrame(() => {
      //console.log("rendering");
      const lastT = this.lastT.get();
      const currentT = new Date().getTime();
      this.lastT.set(currentT);
      const dT = currentT - lastT;

      const lastX = this.lastTranslationX.get();
      const currentX = this.translationX.get();
      this.lastTranslationX.set(currentX);
      const dX = currentX - lastX;
      const locked = Session.get("translationLocked");
      if (locked) {
        const currentV = dT !== 0 ? (dX / dT) : 0;
        this.lastV.set(currentV);
        //console.log("locked");
      }

      const current = Session.get("home.currentSlide");


      this.requestedAnimationFrame = null;
      this.updateSliderTranslation(currentX);

      if (!locked) {
        const x1 = -(current  - 1) * slider.clientWidth / Session.get("home.slidesLength");
        const x0 = this.X0;
        const v0 = this.V0;
        const t1 = 500;
        const k0 = 6 * (v0 - 2 * (x1 - x0) / t1) / (t1 * t1);
        const a0 = -(k0 * t1 / 2 + v0 / t1);

        if (this.translationX.get() !== x1) {
          const time = new Date().getTime() - this.T0;
          if (time >= t1) {
            this.translationX.set(x1);
            this.lastV.set(0);
            //console.log("end");
            return;
          }
          const x = x0 + v0 * time + a0 * time * time / 2 + k0 * time * time * time / 6;
          this.translationX.set(x);
          const v = v0 + a0 * time + k0 * time * time / 2;
          this.lastV.set(v);
          c.invalidate();
        }
      }

    });
  });
});

Template.home.onDestroyed(function() {
  window.removeEventListener("hashchange", this.hashchangeHandler, false);
  window.removeEventListener("keyup", this.keyupHandler, false);
  Meteor.clearInterval(this.interval);
});

Template.home.helpers({
  slideWidth() {
    return (100 / Session.get("home.slidesLength")) + "%";
  },
  minWidth() {
    return (100 * Session.get("home.slidesLength")) + "%";
  },
  leftShift() {
    return (-100 * Session.get("home.slidesLength")) + "%";
  },
  slideIndicatorIcons() {
    const current = Session.get("home.currentSlide");
    const length = Session.get("home.slidesLength");
    const result = [];

    for (let i = 1; i <= length; i++) {
      result.push({
        id: i,
        full: i <= current
      });
    }

    return result;
  }
});


Template.home.events({
  "touchstart .slideContainer"(e, t) {
    const x = e.originalEvent.targetTouches[0].screenX;

    t.lockTranslationX.set(t.translationX.get());
    t.touchStartX.set(x);

    t.translationLocked.set(true);
    Session.set("translationLocked", true);
    //console.log("touchstart");
  },
  "touchend .slideContainer"(e, t) {
    t.translationLocked.set(false);
    Session.set("translationLocked", false);
    //console.log("touchend");
  },
  "touchcancel .slideContainer"(e, t) {
    //console.log("touchcancel");
    t.translationLocked.set(false);
    Session.set("translationLocked", false);
  },
  "touchleave .slideContainer"() {
    //console.log("touchleave");
  },
  "touchmove .slideContainer"(e, t) {
    const x = e.originalEvent.targetTouches[0].screenX;
    const result = t.lockTranslationX.get() + x - t.touchStartX.get();
    t.translationX.set(result);
  }
});

Template.slideIndicatorIcon.helpers({
  iconClass() {
    return this.full ? "fa-circle" : "fa-circle-o";
  },
  href() {
    return Session.get("home.slide." + this.id);
  }
});

Template.slideIndicatorIcon.events({
  "click a"() {
    Session.set("home.currentSlide", this.id);
  }
});
