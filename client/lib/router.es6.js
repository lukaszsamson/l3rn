"use strict";

Router.configure({
  layoutTemplate: "applicationLayout",
  notFoundtemplate: "notFound",
  loadingTemplate: "loading"
});

Router.onBeforeAction(function() {
  // all properties available in the route function
  // are also available here such as this.params

  if (!Meteor.userId()) {
    // if the user is not logged in, render the Login template
    this.render("Login");
  } else {
    // otherwise don't hold up the rest of hooks or our route/action function
    // from running
    this.next();
  }
}, {
  only: ["myQuestions"]
    // or except: ['routeOne', 'routeTwo']
});

Router.route("/", {
  name: "home",
  action() {
    this.render("home");
  }
});
Router.route("/myquestions", {
  name: "myQuestions",
  waitOn() {
    return [
      Meteor.subscribe("questions"),
      Meteor.subscribe("images")
    ];
  },

  action() {
    const filter = {};

    for (const prop of ["isPublic", "isDiscoverable", "isClosed", "hasVotes"]) {
      const filterByProp = Session.get(`myQuestions.${prop}`);
      if (typeof filterByProp === "boolean") {
        if (prop === "hasVotes") {
          const gt = {$gt: 0};
          filter["options.votes"] = filterByProp ? gt : {$not: gt};
        } else {
          filter[prop] = filterByProp;
        }
      }
    }

    this.render("myQuestions", {
      data: {
        questions: app.Questions.find(filter)
      }
    });
  }
});
Router.route("/questions/:_id", {
  name: "question",

  waitOn() {
    return [Meteor.subscribe("questions-image", this.params._id),
      Meteor.subscribe("votes-for-question", this.params._id),
      Meteor.subscribe("question-authors", this.params._id),
      Meteor.subscribe("images")
    ];
  },

  action() {
    const question = app.Questions.findOne({
      _id: this.params._id
    });
    if (!question) {
      this.render("questionNotFound");
    } else {
      Session.set("onPublicQuestion", question.isPublic);
      Session.set("question.imageSelectors", [{}, {}]);
      question.options[0].imageFile = app.Images.findOne({_id: question.options[0].imageId});
      question.options[1].imageFile = app.Images.findOne({_id: question.options[1].imageId});
      this.render("question", {
        data: {
          question,
          vote: app.Votes.findOne({
            questionId: this.params._id
          }),
          author: app.QuestionAuthors.findOne({})
        }
      });
    }
  }

});

Router.route("/next", {
  name: "nextQuestion",

  waitOn() {
    return Meteor.subscribe("questions-to-answer");
  },

  action() {
    const question = app.QuestionsToAnswer.findOne({});
    if (!question) {
      this.render("noMoreQuestions");
    } else {
      this.redirect("question", {
        _id: question.questionId
      });
    }
  }

});

Router.route("/create", {
  name: "create",
  action() {
    this.render("questionCreator", {
      data() {
        return {
          imageSelectors: Session.get("questionCreator.imageSelectors"),
          name: Session.get("questionCreator.name")
        };
      }
    });
  }
});
