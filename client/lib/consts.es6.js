"use strict";

app.getImageBackgroundStyle = function(image) {
  return {
    style: image ? `background-image: url("${image}")` : ""
  };
};
