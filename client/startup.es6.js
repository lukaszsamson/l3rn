"use strict";

Meteor.startup(() => {
  let userWasLoggedIn = false;
  Deps.autorun(() => {
    if (!Meteor.userId()) {
      if (userWasLoggedIn) {
        Session.set("questionCreator.name", "");
        Session.set("questionCreator.imageSelectors", app.getDefaultImageSelectors());
        Session.set("questionCreator.creating", false);
        Session.set("questionCreator.progressCurrentHeader", 1);

        if (!(Router.current().route.getName() === "question" && Session.get("onPublicQuestion"))) {
          Router.go("home");
        }
      }
    }
    else {
      userWasLoggedIn = true;
    }
  });

  Session.setDefault("questionCreator.name", "");
  Session.setDefault("questionCreator.imageSelectors", app.getDefaultImageSelectors());


  const mediaQuery = window.matchMedia("(max-width: 894px)");
  const mediaQueryListener = (mq) => {
    Session.set("ui.phoneMode", mq.matches);
  };
  mediaQuery.addListener(mediaQueryListener);
  mediaQueryListener(mediaQuery);

  if (navigator.userAgent.indexOf("MSIE") !== -1 || navigator.appVersion.indexOf("Trident/") > 0) {
    Meteor.setInterval(() => {
      const images = Array.prototype.slice.call(document.querySelectorAll(".img img"));
      if (images.length < 1) {
        return;
      }
      const max = Math.max(...images.map((i) => i.height));

      images.forEach((i) => {
        const height = Session.equals("ui.phoneMode", true) ? i.height : max;
        if (!isNaN(height) && height > 0) {
          i.parentElement.style.height = height + "px";
        } else {
          delete i.parentElement.style.height;
        }
      });
    }, 1000);
  }
});
