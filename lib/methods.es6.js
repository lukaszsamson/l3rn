/*eslint new-cap:0*/

"use strict";

const authorize = (f) => (...args) => {
  if (!Meteor.userId()) {
    throw new Meteor.Error("not-authorized");
  }

  const result = f.apply(this, args);
  return result;
};

const nonEmptyString = (max) => Match.Where((x) => {
  check(x, String);
  return x.length > 0 && x.length <= max;
});

const optionalString = (max) => Match.Where((x) => {
  check(x, String);
  return x.length <= max;
});

Meteor.methods({
  create: authorize(function(name, options) {
    check(name, nonEmptyString(160));
    check(options, [{
      id: Match.Integer,
      description: optionalString(160),
      imageId: String
    }]);

    if (options.length !== 2) {
      throw new Meteor.Error("expected 2 options");
    }

    //todo needed?
    if (Meteor.isServer) {
      const images = app.Images.find({
        _id: {
          $in: options.map(p => p.imageId)
        }
      }, {
      }).fetch();
      if (images.length !== 2) {
        throw new Meteor.Error("expected 2 images");
      }
    }

    const question = {
      name,
      options,
      created: new Date(),
      isPublic: false,
      isDiscoverable: false,
      isClosed: false,
      owner: Meteor.userId()
    };
    const id = app.Questions.insert(question);

    app.Images.update({
      _id: {
        $in: options.map(p => p.imageId)
      }
    }, {
      $set: {
        questionId: id,
        isPublic: false,
        owner: Meteor.userId()
      }
    }, {
      multi: true
    });

    return id;
  }),

  vote: authorize(function(questionId, optionId) {
    check(questionId, nonEmptyString(30));
    check(optionId, Match.Integer);

    const vote = app.Votes.findOne({
      questionId,
      userId: Meteor.userId()
    });
    if (vote) {
      throw new Meteor.Error("already-voted");
    }

    const question = app.Questions.findOne({_id: questionId});

    if (question.isClosed) {
      throw new Meteor.Error("question-closed");
    }

    app.Votes.insert({
      questionId,
      userId: Meteor.userId(),
      date: new Date(),
      optionId
    });
    app.Questions.update({
      _id: questionId,
      "options.id": optionId
    }, {
      $inc: {
        "options.$.votes": 1
      }
    });
  }),
  remove: authorize(function(questionId) {
    check(questionId, nonEmptyString(30));
    const removed = new Date();
    app.Questions.update({
      _id: questionId,
      owner: Meteor.userId()
    }, {
      $set: {
        removed
      }
    });

    app.Images.update({
      questionId
    }, {
      $set: {
        removed
      }
    }, {
      multi: true
    });
  }),

  updatePrivacySettings: authorize(function(questionId, settings) {
    check(questionId, nonEmptyString(30));
    check(settings, {
      isPublic: Boolean,
      isDiscoverable: Boolean
    });

    const question = app.Questions.findOne({_id: questionId});

    if (!settings.isPublic || question.isClosed) {
      settings.isDiscoverable = false;
    }

    app.Questions.update({
      _id: questionId,
      owner: Meteor.userId()
    }, {
      $set: settings
    });

    app.Images.update({
      questionId
    }, {
      $set: {
        isPublic: settings.isPublic
      }
    }, {
      multi: true
    });
  }),

  close: authorize(function(questionId) {
    check(questionId, nonEmptyString(30));
    app.Questions.update({
      _id: questionId,
      owner: Meteor.userId()
    }, {
      $set: {
        isClosed: true,
        closedDate: new Date()
      }
    });
  })
});
