"use strict";

app.Questions = new Mongo.Collection("questions");
app.Votes = new Mongo.Collection("votes");

//todo read from env
const path = "~/uploads";

const createThumb = (fileObj, readStream, writeStream) => {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name())
    //todo .limit() resources
    .autoOrient()
    .resize(150, 150)
    .quality(80)
    .stream()
    .pipe(writeStream);
};

const createOptimized = (fileObj, readStream, writeStream) => {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name())
    //todo .limit() resources
    .resize(1500, 1500, ">")
    .autoOrient()
    .quality(90)
    .stream()
    .pipe(writeStream);
};

const createFull = (fileObj, readStream, writeStream) => {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name())
    //todo .limit() resources
    .autoOrient()
    .quality(95)
    .stream()
    .pipe(writeStream);
};

app.Images = new FS.Collection("images", {
  filter: {
    maxSize: 10 * 1024 * 1024, // in bytes
    //todo png
    allow: {
      contentTypes: ["image/jpeg"],
      extensions: ["jpg"]
    },
    onInvalid: function (message) {
      //todo errorhandling
      if (Meteor.isClient) {
        //alert(message);
        throw new Error(message);
      } else {
        console.log(message);
      }
    }
  },
  stores: [
    new FS.Store.FileSystem("thumbs", {
      transformWrite: createThumb,
      path: path + "/thumbs"
    }),
    new FS.Store.FileSystem("images", {
      transformWrite: createFull,
      path: path + "/full"
    }),
    new FS.Store.FileSystem("imagesOptimized", {
      transformWrite: createOptimized,
      path: path + "/optimized"
    })
  ]
});
