"use strict";

app.Images.allow({
  insert: () => true,
  update: () => true,
  remove: () => false,
  download: (userId, fileObj) => {
    return (fileObj.isPublic || (userId && fileObj.owner === userId)) && !fileObj.removed;
  }
});
