"use strict";

Meteor.publish("votes-for-question", function(questionId) {
  check(questionId, String);

  return app.Votes.find({
    userId: this.userId,
    questionId
  });
});

Meteor.publish("images", function() {
  //check(questionId, String);
  return app.Images.find({
    //questionId,
    removed: {
      $exists: false
    }//,
    // $or: [{
    //   isPublic: true
    // }, {
    //   owner: this.userId
    // }]
  }, {
  });
});

Meteor.publish("questions", function() {
  return app.Questions.find({
    owner: this.userId,
    removed: {
      $exists: false
    }
  }, {
    fields: {
      "options.image": 0,
      owner: 0
    },
    sort: [["created", "desc"]]
  });
});

Meteor.publish("question-authors", function(questionId) {
  check(questionId, String);
  const question = app.Questions.findOne({
    _id: questionId
  }, {
    fields: {
      owner: 1
    }
  });

  if (question) {
    const user = Meteor.users.findOne({
      _id: question.owner
    }, {
      // fields: {
      //   "emails.address": 1
      // }
    });

    this.added("questionAuthors", user._id, {
      name: user.services.facebook ? user.services.facebook.name : "",
      email: user.emails ? user.emails[0].address : ""
    });
  }
  this.ready();
});

Meteor.publish("questions-image", function(questionId) {
  check(questionId, String);

  return app.Questions.find({
    _id: questionId,
    removed: {
      $exists: false
    },
    $or: [{
      isPublic: true
    }, {
      owner: this.userId
    }]
  }, {});
});

Meteor.publish("questions-to-answer", function() {
  const votes = app.Votes.find({
    userId: this.userId
  });

  const answeredQuestionsIds = votes.map(v => v.questionId);

  const question = app.Questions.findOne({
    removed: {
      $exists: false
    },
    isClosed: false,
    $or: [{
      isPublic: true,
      isDiscoverable: true
    }, {
      owner: this.userId
    }],
    _id: {
      $nin: answeredQuestionsIds
    }
  }, {});

  if (question) {
    this.added("questionsToAnswer", Random.id(), {
      questionId: question._id
    });
  }

  this.ready();
});
